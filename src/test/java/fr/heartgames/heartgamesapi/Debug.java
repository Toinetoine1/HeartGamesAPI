package fr.heartgames.heartgamesapi;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Random;

/**
 * Created by Toinetoine1 on 03/08/2019.
 */


public class Debug {

    private static Random RANDOM = new Random();

    public static void main(String[] args) {
        try {
            URL url = new URL("https://jenkins.heartgames.fr/job/HeartGamesAPI/build"); // Jenkins URL localhost:8080, job named 'test'
            String user = "auto"; // username
            String pass = "114099617efcd398bde9003e1924cd7c7d"; // password or API token
            String authStr = user + ":" + pass;
            String encoding = Base64.getEncoder().encodeToString(authStr.getBytes(StandardCharsets.UTF_8));

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.addRequestProperty("User-Agent", "Mozilla/4.76");
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            InputStream content = connection.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean isBetween(int min, int max, int current){
        if (current >= min && current <= max) {
            System.out.println("return true");
            return true;
        }

        System.out.println("return false");
        return false;
    }


}
