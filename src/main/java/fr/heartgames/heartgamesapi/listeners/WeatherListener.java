package fr.heartgames.heartgamesapi.listeners;

import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.utils.HeartListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.weather.WeatherChangeEvent;

/**
 * Created by Toinetoine1 on 21/10/2019.
 */

public class WeatherListener extends HeartListener {

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event){
        if(!event.toWeatherState())
            return;

        if(GameAPI.getAPI().getConfig().getBoolean("disable_storm")){
            event.setCancelled(true);
            event.getWorld().setThundering(false);
            event.getWorld().setThunderDuration(0);
        }

        if(GameAPI.getAPI().getConfig().getBoolean("disable_rain")){
            event.setCancelled(true);
            event.getWorld().setWeatherDuration(0);
        }
    }

}
