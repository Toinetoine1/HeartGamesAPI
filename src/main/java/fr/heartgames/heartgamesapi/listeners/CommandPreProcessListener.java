package fr.heartgames.heartgamesapi.listeners;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.logs.Action;
import fr.heartgames.heartgamesapi.logs.LogManager;
import fr.heartgames.heartgamesapi.utils.HeartListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created by Toinetoine1 on 21/11/2019.
 */

public class CommandPreProcessListener extends HeartListener {

    @EventHandler
    public void onCommandPreProcess(PlayerCommandPreprocessEvent event){
        HeartGamesPlayer player = (HeartGamesPlayer) event.getPlayer();
        String command = event.getMessage();

        if(player.isStaff()){
            System.out.println("Add log !");
            LogManager.log(player.getName(), Action.COMMAND, command);
        }
    }

}
