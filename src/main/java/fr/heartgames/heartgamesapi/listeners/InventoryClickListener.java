package fr.heartgames.heartgamesapi.listeners;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.inventory.ClickAction;
import fr.heartgames.heartgamesapi.inventory.CustomHolder;
import fr.heartgames.heartgamesapi.inventory.Icon;
import fr.heartgames.heartgamesapi.utils.HeartListener;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Toinetoine1 on 12/10/2019.
 */

public class InventoryClickListener extends HeartListener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            HeartGamesPlayer player = (HeartGamesPlayer) event.getWhoClicked();

            if (event.getView().getTopInventory().getHolder() instanceof CustomHolder) {
                event.setCancelled(true);

                ItemStack current = event.getCurrentItem();
                ItemStack cursor = event.getCursor();
                if (event.getRawSlot() == -999) return;

                CustomHolder customHolder = (CustomHolder) event.getView().getTopInventory().getHolder();


                Icon icon = customHolder.getIcon(event.getRawSlot());
                if (icon == null) return;

                for (ClickAction clickAction : icon.getClickActions()) {
                    clickAction.execute(player, current);
                }
            }
        }


    }

}
