package fr.heartgames.heartgamesapi.listeners;

import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.permissions.PermissionManager;
import fr.heartgames.heartgamesapi.permissions.groups.Group;
import fr.heartgames.heartgamesapi.utils.HeartListener;
import fr.heartgames.heartgamesapi.utils.general.BukkitUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.*;

/**
 * Created by Toinetoine1 on 19/08/2019.
 */

public class TabGroupListener extends HeartListener {

    public static boolean enabled = false;
    private Map<Group, String> maps = new HashMap<>();

    public TabGroupListener() {
        maps.clear();

        Collection<Group> groups = PermissionManager.getInstance().getGroups().values();
        List<Group> linkedList = new LinkedList<>(groups);

        linkedList.sort((perm1, perm2) -> Integer.compare(perm2.getPower(), perm1.getPower()));

        for (int i = 0; i < linkedList.size(); i++) {
            Group Group = linkedList.get(i);
            String encrypted = encrypt(i);

            maps.put(Group, encrypted);
        }

        Bukkit.getScheduler().runTaskTimer(GameAPI.getAPI(), run(), 20, 20 * 10);
    }

    private Runnable run() {
        return () -> BukkitUtils.getAllPlayers().forEach(this::update);
    }

    private void update(HeartGamesPlayer player) {
        Scoreboard scoreboard = player.getScoreboard();

        if (scoreboard == null) {
            scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            return;
        }

        for (HeartGamesPlayer otherPlayer : BukkitUtils.getAllPlayers()) {
            for (Map.Entry<Group, String> entry : maps.entrySet()) {
                Team team = scoreboard.getTeam(entry.getValue());

                if (team == null) {
                    team = scoreboard.registerNewTeam(entry.getValue());
                    team.setPrefix(entry.getKey().getLinkedTo().getFormattedName());
                }

                boolean inIt = entry.getKey().equals(otherPlayer.getMainGroup());

                if (!team.hasPlayer(otherPlayer)) {
                    if (inIt) {
                        team.addPlayer(otherPlayer);
                    }
                } else {
                    if (!inIt) {
                        team.removePlayer(otherPlayer);
                    }
                }
            }
        }

    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if(!enabled)
            return;

        HeartGamesPlayer player = (HeartGamesPlayer) event.getPlayer();

        player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
    }

    private static String encrypt(int id) {
        int firstId = Math.floorDiv(id, 26);
        char firstLetter = generateForId(firstId);
        int leftovers = id % 26;
        char leftover = generateForId(leftovers);
        return firstLetter + "" + leftover;
    }

    private static char generateForId(int id) {
        int A = 'A';
        if (id > 26) {
            A = 'a';
            id -= 26;
            return (char) (A + id);
        }

        return (char) (A + id);
    }

}
