package fr.heartgames.heartgamesapi.listeners;

import fr.heartgames.heartgamesapi.players.GameHeartGamesPlayer;
import fr.heartgames.heartgamesapi.utils.HeartListener;
import fr.heartgames.heartgamesapi.utils.reflections.ReflectionUtils;
import fr.heartgames.heartgamesapi.utils.reflections.Reflector;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * Created by Toinetoine1 on 04/08/2019.
 */

public class LoginListener extends HeartListener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onLogin(PlayerLoginEvent event){
        Reflector reflector = new Reflector(ReflectionUtils.getHandle(event.getPlayer()));
        GameHeartGamesPlayer player = null;
        try {
            player = new GameHeartGamesPlayer((CraftServer) Bukkit.getServer(), (EntityPlayer) reflector.getReflected());
            reflector.setFieldValue("bukkitEntity", player);
        } catch (Exception e) {
            System.out.println("Impossible de modifier la classe joueur !");
            e.printStackTrace();
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "§cImpossible de vous connectez à ce serveur !");
            return;
        }


    }

}
