package fr.heartgames.heartgamesapi.commands;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.utils.AbstractCommand;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Toinetoine1 on 15/08/2019.
 */

public class GameModeCommand extends AbstractCommand {

    public GameModeCommand() {
        super("gamemode", HeartGamesPlayer.GamePermission.BMODERATOR, "gm");
        this.allowConsole(false);
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        HeartGamesPlayer player = (HeartGamesPlayer) sender;

        if (args.length == 0) {
            player.sendMessage("§cUsage: §6/gamemode <0/1/2/3>");
        } else if (args.length == 1) {
            int i = -1;

            try {
                i = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                player.sendMessage("§cVous devez saisir un nombre");
            }

            switch (i) {
                case 0:
                    player.setGameMode(GameMode.SURVIVAL);
                    player.sendMessage("§cVous êtes maintenant en mode §9Survie");
                    break;
                case 1:
                    player.setGameMode(GameMode.CREATIVE);
                    player.sendMessage("§cVous êtes maintenant en mode §9Créatif");
                    break;
                case 2:
                    player.setGameMode(GameMode.SPECTATOR);
                    player.sendMessage("§cVous êtes maintenant en mode §9Spectateur");
                    break;
                case 3:
                    player.setGameMode(GameMode.ADVENTURE);
                    player.sendMessage("§cVous êtes maintenant en mode §9Aventure");
                default:
                    player.sendMessage("§cVeuillez entrer un nombre correct");
                    break;
            }
        } else if (args.length == 2) {
            Player target = Bukkit.getPlayer(args[1]);
            if (target == null) {
                player.sendMessage("§cJoueur introuvable");
            } else {
                int i = -1;

                try {
                    i = Integer.parseInt(args[0]);
                } catch (NumberFormatException e) {
                    player.sendMessage("§cVous devez saisir un nombre");
                }

                if (target.getName().equals(player.getName())) {
                    player.sendMessage("Enlever votre nom pour éxécuter cette commande");
                    return;
                }

                switch (i) {
                    case 0:
                        target.setGameMode(GameMode.SURVIVAL);
                        player.sendMessage("§cLe joueur "+target.getName()+" est maintenant en §9Survie");
                        player.sendMessage("§cVous êtes maintenant en §9Survie");
                        break;
                    case 1:
                        target.setGameMode(GameMode.CREATIVE);
                        player.sendMessage("§cLe joueur "+target.getName()+" est maintenant en §9Créatif");
                        player.sendMessage("§cVous êtes maintenant en §9Créatif");
                        break;
                    case 2:
                        target.setGameMode(GameMode.SPECTATOR);
                        player.sendMessage("§cLe joueur "+target.getName()+" est maintenant en §9Spectateur");
                        player.sendMessage("§cVous êtes maintenant en §9Spectateur");
                        break;
                    case 3:
                        target.setGameMode(GameMode.ADVENTURE);
                        player.sendMessage("§cLe joueur "+target.getName()+" est maintenant en §9Aventure");
                        player.sendMessage("§cVous êtes maintenant en §9Aventure");
                    default:
                        player.sendMessage("§cVeuillez entrer un nombre correct");
                        break;
                }
            }
        }

    }
}
