package fr.heartgames.heartgamesapi.commands;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.utils.AbstractCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Toinetoine1 on 15/08/2019.
 */


public class FlyCommand extends AbstractCommand {

    public FlyCommand() {
        super("fly", HeartGamesPlayer.GamePermission.BMODERATOR);
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        HeartGamesPlayer player = (HeartGamesPlayer) sender;

        if (args.length == 0) {
            if (!player.getAllowFlight()) {
                player.setAllowFlight(true);
                player.sendMessage("§cMode superman activé");
            } else {
                player.setAllowFlight(false);
                player.sendMessage("§cMode superman désactivé");
            }
        }
        if (args.length == 1) {
            Player target = Bukkit.getPlayerExact(args[0]);
            if (target == null) {
                player.sendMessage("§cJoueur introuvable");
                return;
            }

            if(target.getName().equals(player.getName())){
                player.sendMessage("Enlever votre nom pour éxécuter cette commande");
                return;
            }

            if (!target.getAllowFlight()) {
                target.setAllowFlight(true);
                player.sendMessage("§cLe joueur "+target.getName()+" peut maintenant voler");
                player.sendMessage("§cMode superman activé");
            } else {
                target.setAllowFlight(false);
                player.sendMessage("§cLe joueur "+target.getName()+" ne peut plus voler");
                player.sendMessage("§cMode superman désactivé");
            }
        }

    }
}
