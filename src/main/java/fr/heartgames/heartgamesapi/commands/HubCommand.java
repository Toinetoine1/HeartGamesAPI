package fr.heartgames.heartgamesapi.commands;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.utils.AbstractCommand;
import org.bukkit.command.CommandSender;

/**
 * Created by Toinetoine1 on 15/08/2019.
 */


public class HubCommand extends AbstractCommand {

    public HubCommand() {
        super("hub", HeartGamesPlayer.GamePermission.PLAYER, "lobby");
        this.allowConsole(false);
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        HeartGamesPlayer heartGamesPlayer = (HeartGamesPlayer) sender;

        heartGamesPlayer.sendMessage("§cConnexion au hub..");
        heartGamesPlayer.sendPlayer("hub");
    }
}
