package fr.heartgames.heartgamesapi.utils.reflections.helper.resolver.minecraft;

import fr.heartgames.heartgamesapi.utils.reflections.helper.minecraft.Minecraft;
import fr.heartgames.heartgamesapi.utils.reflections.helper.resolver.ClassResolver;

public class NMSClassResolver extends ClassResolver {

	@Override
	public Class resolve(String... names) throws ClassNotFoundException {
		for (int i = 0; i < names.length; i++) {
			if (!names[i].startsWith("net.minecraft.server")) {
				names[i] = "net.minecraft.server." + Minecraft.getVersion() + names[i];
			}
		}
		return super.resolve(names);
	}
}
