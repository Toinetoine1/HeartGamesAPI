package fr.heartgames.heartgamesapi.utils;

import fr.heartgames.heartgamesapi.api.GameAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * Created by Toinetoine1 on 03/08/2019.
 */


public class Utils {

    public static void info(String message) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    public static void severe(String message) {
        GameAPI.getAPI().getLogger().severe(ChatColor.translateAlternateColorCodes('&', message));
    }

    public static String buildString(String[] args, int begin){
        StringBuilder builder = new StringBuilder();
        for(int i = begin; i < args.length; i++){
            builder.append(args[i]).append(" ");
        }
        return builder.toString();
    }

}
