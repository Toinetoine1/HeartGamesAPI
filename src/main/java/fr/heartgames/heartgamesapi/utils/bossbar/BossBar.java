package fr.heartgames.heartgamesapi.utils.bossbar;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.Collection;

public interface BossBar {

	/**
	 * @return The players which can see the BossBar
	 */
	Collection<? extends Player> getPlayers();

	/**
	 * Add a player to this BossBar
	 *
	 * @param player {@link Player} to add
	 */
	void addPlayer(Player player);

	/**
	 * Remove a player from this BossBar
	 *
	 * @param player {@link Player} to remove
	 */
	void removePlayer(Player player);

	BossBarAPI.Color getColor();

	void setColor(BossBarAPI.Color color);

	BossBarAPI.Style getStyle();

	void setStyle(BossBarAPI.Style style);

	void setProperty(BossBarAPI.Property property, boolean flag);

	String getMessage();

	/**
	 * @param flag whether the BossBar is visible
	 */
	void setVisible(boolean flag);

	/**
	 * @return whether the BossBar is visible
	 */
	boolean isVisible();

	/**
	 * @return the progress (0.0 - 1.0)
	 */
	float getProgress();

	/**
	 * @param progress the new progress (0.0 - 1.0)
	 */
	void setProgress(float progress);

	@Deprecated
	float getMaxHealth();

	@Deprecated
	void setHealth(float percentage);

	@Deprecated
	float getHealth();

	@Deprecated
	void setMessage(String message);

	@Deprecated
	Player getReceiver();

	@Deprecated
	Location getLocation();

	@Deprecated
	void updateMovement();
}
