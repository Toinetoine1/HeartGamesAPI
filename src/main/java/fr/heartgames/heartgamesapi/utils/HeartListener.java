package fr.heartgames.heartgamesapi.utils;

import fr.heartgames.heartgamesapi.api.GameAPI;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

/**
 * Created by Toinetoine1 on 04/08/2019.
 */

public abstract class HeartListener implements Listener {

    public HeartListener() {
        Bukkit.getPluginManager().registerEvents(this, GameAPI.getAPI());
    }
}
