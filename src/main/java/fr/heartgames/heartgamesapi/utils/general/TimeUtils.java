package fr.heartgames.heartgamesapi.utils.general;

/**
 * Created by Toinetoine1 on 23/08/2019.
 */

public class TimeUtils {

    public static long getTime() {
        return System.currentTimeMillis();
    }

    public static long toMilliSec(String time) {
        char[] array = time.toCharArray();
        System.out.println("format: "+format(time));

        switch (array[array.length - 1]) {
            case 's':
                return format(time) * 1000L;
            case 'm':
                return format(time) * 1000L * 60;
            case 'h':
                return format(time) * 1000L * 60 * 60;
            case 'd':
            case 'j':
                return format(time) * 1000L * 60 * 60 * 24;
            case 'M':
                return format(time) * 1000L * 60 * 60 * 24 * 30;
            default:
                return -1;
        }
    }

    private static int format(String timeString) {
        boolean started = false;
        boolean negative = false;
        int result = 0;
        try {
            for (char c : timeString.toCharArray()) {
                if (!started && Character.isWhitespace(c)) {
                } else if (!started && (c == '+' || c == '-')) {
                    negative = c == '-';
                    started = true;
                } else if (Character.isDigit(c)) {
                    result = Math.multiplyExact(result, 10);
                    result = Math.addExact(result, Character.getNumericValue(c));
                    started = true;
                } else {
                    break;
                }
            }
        } catch (ArithmeticException e) {
            return negative ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        }
        return negative ? -result : result;
    }

}
