package fr.heartgames.heartgamesapi.utils.general;

import com.google.common.base.Charsets;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Toinetoine1 on 03/08/2019.
 */


public class FileUtils {

    public static String readFile(File file){

        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = Files.newBufferedReader(Paths.get("filename.txt"))) {

            // read line by line
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    public static void save(File file, String toSave) {
        try {
            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), Charsets.UTF_8.name());
            writer.write(toSave);
            writer.flush();
            writer.close();
        } catch (IOException var3) {
            var3.printStackTrace();
        }

    }


}
