package fr.heartgames.heartgamesapi.utils.general;

import fr.heartgames.heartgamesapi.api.GameAPI;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

/**
 * Created by Toinetoine1 on 03/08/2019.
 */

public class YMLUtils {

    public static YamlConfiguration loadFile(File file){
        if(!file.exists()){
            GameAPI.getAPI().saveResource(file.getName(), false);
        }

        return YamlConfiguration.loadConfiguration(file);
    }



}
