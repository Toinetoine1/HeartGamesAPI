package fr.heartgames.heartgamesapi.utils.general;

import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.utils.AbstractCommand;
import fr.heartgames.heartgamesapi.utils.HeartListener;
import org.apache.commons.lang3.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public class BukkitUtils {

    public static void forEachPlayers(Consumer<HeartGamesPlayer> action) {
        getAllPlayers().forEach(action);
    }

    public static List<HeartGamesPlayer> getAllPlayers() {
        return GameAPI.getAPI().getOnlinePlayers();
    }


    public static HeartGamesPlayer getPlayer(String playerName) {
        Player player = Bukkit.getPlayer(playerName);
        if (player == null) return null;
        return (HeartGamesPlayer) player;
    }

    public static HeartGamesPlayer getPlayer(UUID playerUniqueId) {
        Player player = Bukkit.getPlayer(playerUniqueId);
        if (player == null) return null;
        return (HeartGamesPlayer) player;
    }

    public static void instanciateListenersAndCommandsFrom(Plugin plugin, String... paths) throws IOException {
        URL url = plugin.getClass().getProtectionDomain().getCodeSource().getLocation();

        ZipInputStream zip = new ZipInputStream(url.openStream());
        ZipEntry entry = null;

        while((entry = zip.getNextEntry()) != null)
        {
            String finded = null;

            for(String path : paths){
                if(entry.getName().startsWith( path.replace(".", "/") ))
                {
                    finded = entry.getName().replace("/", ".");
                    break;
                }}

            if(finded != null && entry.getName().endsWith(".class"))
            {
                try {
                    String className = finded.substring(0, finded.length() - 6);

                    Class<?> clazz = plugin.getClass().getClassLoader().loadClass(className);

                    if( inheritFrom(clazz, HeartListener.class) )
                    {
                        instanciate(clazz);
                    }
                    else if( inheritNormalListener(clazz) )
                    {
                        Listener listener = (Listener) instanciate(clazz);

                        if(listener != null)
                            plugin.getServer().getPluginManager().registerEvents(listener, plugin);
                    }
                    else if( inheritFrom(clazz, AbstractCommand.class) )
                    {
                        instanciate(clazz);
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            }

        }
    }

    private static boolean inheritFrom(Class<?> clazz, Class<?> from){
        while(clazz != Object.class)
        {
            if(clazz == from)
                return true;

            clazz = clazz.getSuperclass();
        }

        return false;
    }

    private static boolean inheritNormalListener(Class<?> clazz){

        while(clazz != Object.class)
        {
            if(ArrayUtils.contains(clazz.getInterfaces(), Listener.class))
                return true;

            clazz = clazz.getSuperclass();
        }

        return false;

    }

    private static Object instanciate(Class<?> clazz) throws Exception {
        try {
            return clazz.getConstructor().newInstance();
        } catch(NoSuchMethodException e){
            return null;
        }
    }

}
