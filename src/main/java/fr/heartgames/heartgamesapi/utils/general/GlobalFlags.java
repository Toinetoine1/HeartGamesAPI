package fr.heartgames.heartgamesapi.utils.general;

import java.util.HashMap;

/**
 * Created by Toinetoine1 on 21/08/2019.
 */

public class GlobalFlags {

    private static HashMap<Object, Long> map = new HashMap<>();

    public static void set(Object object, long addedTime) {
        long time = System.currentTimeMillis() + addedTime;

        map.put(object, time);
    }

    public static boolean has(Object object) {
        if (!map.containsKey(object)) {
            return false;
        } else {
            return map.get(object) > System.currentTimeMillis();
        }
    }

}
