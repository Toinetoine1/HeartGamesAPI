package fr.heartgames.heartgamesapi.utils.general;

import fr.heartgames.heartgamesapi.utils.Cuboid;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class ConfigUtils {

    public static String convertLocationBlockToString(Location location) {
        if(location == null)
            return "null";
        String world = location.getWorld().getName();
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();
        return world + "," + x + "," + y + "," + z;
    }

    public static String convertLocationToString(Location location) {
        if(location == null)
            return "null";
        String world = location.getWorld().getName();
        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        float pitch = location.getPitch();
        float yaw = location.getYaw();
        return world + "," + x + "," + y + "," + z + "," + yaw + "," + pitch;
    }

    public static Location convertStringToBlockLocation(String string) {
        if (string != null) {
            String[] wxyz = string.split(",");
            World w = Bukkit.getWorld(wxyz[0]);
            int x = Integer.parseInt(wxyz[1]);
            int y = Integer.parseInt(wxyz[2]);
            int z = Integer.parseInt(wxyz[3]);
            return new Location(w, x, y, z);
        }
        return null;
    }

    public static Location convertStringToLocation(String string) {
        if (string == null || string.equalsIgnoreCase("null"))
            return null;
        String[] wxyzPitchYaw = string.split(",");
        World w = Bukkit.getWorld(wxyzPitchYaw[0]);
        double x = Double.parseDouble(wxyzPitchYaw[1]);
        double y = Double.parseDouble(wxyzPitchYaw[2]);
        double z = Double.parseDouble(wxyzPitchYaw[3]);
        float yaw = Float.parseFloat(wxyzPitchYaw[4]);
        float pitch = Float.parseFloat(wxyzPitchYaw[5]);
        Location location = new Location(w, x, y, z);
        location.setPitch(pitch);
        location.setYaw(yaw);
        return location;
    }

    @SuppressWarnings("unchecked")
    public static <T> T get(Plugin plugin, String key, T defaultValue) {
        if (!plugin.getConfig().contains(key)) {
            plugin.getConfig().set(key, defaultValue);
            plugin.saveConfig();
        }
        return (T) plugin.getConfig().get(key);
    }

    public static Location getBlockLocationFromFile(Plugin plugin, String node) {
        return convertStringToBlockLocation(plugin.getConfig().getString(node));
    }

    public static Cuboid getCuboid(Plugin plugin, String node) {
        return new Cuboid((convertStringToBlockLocation(plugin.getConfig().getString(node + ".location1"))), (convertStringToBlockLocation(plugin.getConfig().getString(node + ".location2"))));
    }

    public static void setCuboid(Plugin plugin, String node, Cuboid cuboid){
        plugin.getConfig().set(node+".location1", convertLocationBlockToString(cuboid.getPoint1()));
        plugin.getConfig().set(node+".location2", convertLocationBlockToString(cuboid.getPoint2()));
        plugin.saveConfig();
    }

    public static boolean getBoolean(Plugin plugin, String key) {
        if (!plugin.getConfig().contains(key)) {
            plugin.getConfig().set(key, false);
            plugin.saveConfig();
            return false;
        }
        return plugin.getConfig().getBoolean(key, false);
    }

    public static int getInt(Plugin plugin, String key) {
        if (!plugin.getConfig().contains(key)) {
            plugin.getConfig().set(key, 0);
            plugin.saveConfig();
            return 0;
        }
        return plugin.getConfig().getInt(key, 0);
    }

    public static Location getLocation(Plugin plugin, String name) {
        return getLocationFromFile(plugin, name);
    }

    public static Location getLocationFromFile(Plugin plugin, String node) {
        return convertStringToLocation(plugin.getConfig().getString(node));
    }

    public static List<Location> getLocationList(Plugin plugin, String name) {
        List<Location> spawns = new ArrayList<>();
        List<String> configSpawns = plugin.getConfig().getStringList(name);
        for (String loc : configSpawns) {
            String[] wxyz = loc.split(",");
            World w = Bukkit.getWorld(wxyz[0]);
            double x = Double.parseDouble(wxyz[1]);
            double y = Double.parseDouble(wxyz[2]);
            double z = Double.parseDouble(wxyz[3]);
            float yaw = Float.parseFloat(wxyz[4]);
            float pitch = Float.parseFloat(wxyz[5]);
            Location location = new Location(w, x, y, z);
            location.setPitch(pitch);
            location.setYaw(yaw);
            spawns.add(location);
        }
        return spawns;
    }

    public static String getMapName(Plugin plugin) {
        return (plugin.getConfig().getString("mapName") != null ? plugin.getConfig().getString("mapName") : "NoName");
    }

    public static Material getMaterial(Plugin plugin, String key) {
        if (!plugin.getConfig().contains(key)) {
            plugin.getConfig().set(key, Material.STONE.name());
            plugin.saveConfig();
            return Material.AIR;
        }
        return Material.getMaterial(plugin.getConfig().getString(key, Material.STONE.name()));
    }

    public static List<String> getStringList(Plugin plugin, String key) {
        if (!plugin.getConfig().contains(key)) {
            List<String> list = new ArrayList<>();
            plugin.getConfig().set(key, list);
            plugin.saveConfig();
            return list;
        }
        return plugin.getConfig().getStringList(key);
    }

    public static List<Integer> getIntList(Plugin plugin, String key) {
        if (!plugin.getConfig().contains(key)) {
            List<Integer> list = new ArrayList<>();
            plugin.getConfig().set(key, list);
            plugin.saveConfig();
            return list;
        }
        return plugin.getConfig().getIntegerList(key);
    }

    public static void saveBlockLocation(Plugin plugin, String node, Location location) {
        plugin.getConfig().set(node, convertLocationBlockToString(location));
        plugin.saveConfig();
    }

    public static void saveLocation(Plugin plugin, String node, Location location) {
        plugin.getConfig().set(node, convertLocationToString(location));
        plugin.saveConfig();
    }

    public static void saveLocationList(Plugin plugin, List<Location> locations, String name) {
        List<String> locsToString = new ArrayList<>();
        for (Location location : locations) {
            locsToString.add(ConfigUtils.convertLocationToString(location));
        }
        plugin.getConfig().set(name, locsToString);
        plugin.saveConfig();
    }

    public static boolean exist(Plugin plugin, String path){
        return plugin.getConfig().get(path) != null;
    }

    public static boolean existCuboid(Plugin plugin, String node){
        return plugin.getConfig().getString(node + ".location1") != null && plugin.getConfig().getString(node + ".location2") != null;
    }

    public static void setLocation(Plugin plugin, Location location, String name) {
        saveLocation(plugin, name, location);
        plugin.saveConfig();
    }
}
