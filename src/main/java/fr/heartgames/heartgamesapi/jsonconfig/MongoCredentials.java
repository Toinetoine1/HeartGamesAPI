package fr.heartgames.heartgamesapi.jsonconfig;

/**
 * Created by Toinetoine1 on 10/08/2019.
 */

public class MongoCredentials {

    private String host;
    private String user;
    private String pass;
    private String databaseName;
    private int port;

    public MongoCredentials(String host, String user, String pass, String databaseName, int port) {
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.databaseName = databaseName;
        this.port = port;
    }

    public String toURL() {
        final StringBuilder sb = new StringBuilder();

        sb.append("mongodb://")
                .append(user)
                .append(":")
                .append(pass)
                .append("@")
                .append(host)
                .append(":")
                .append(port);

        return sb.toString();
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public int getPort() {
        return port;
    }

}
