package fr.heartgames.heartgamesapi.jsonconfig;

import fr.heartgames.heartgamesapi.servers.ServerStatus;
import fr.heartgames.heartgamesapi.servers.ServerType;

/**
 * Created by Toinetoine1 on 05/08/2019.
 */

public class ServerConfig {

    private ServerType serverType;
    private ServerStatus serverStatus;

    public ServerConfig() {
    }

    public ServerConfig(ServerType serverType, ServerStatus serverStatus) {
        this.serverType = serverType;
        this.serverStatus = serverStatus;
    }

    public ServerType getServerType() {
        return serverType;
    }

    public ServerStatus getServerStatus() {
        return serverStatus;
    }

}
