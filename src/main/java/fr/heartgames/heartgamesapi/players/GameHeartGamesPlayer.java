package fr.heartgames.heartgamesapi.players;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.api.players.data.PlayerData;
import fr.heartgames.heartgamesapi.api.scoreboard.CustomObjective;
import fr.heartgames.heartgamesapi.permissions.PermissionManager;
import fr.heartgames.heartgamesapi.permissions.Rank;
import fr.heartgames.heartgamesapi.permissions.groups.Group;
import fr.heartgames.heartgamesapi.players.data.GamePlayerData;
import fr.heartgames.heartgamesapi.tech.DataManager;
import fr.heartgames.heartgamesapi.tech.redis.SaveTag;
import fr.heartgames.heartgamesapi.utils.general.TimeUnit;
import lombok.Getter;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.permissions.PermissionAttachment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Toinetoine1 on 02/08/2019.
 */

public class GameHeartGamesPlayer extends CraftPlayer implements HeartGamesPlayer {

    @Getter
    private static Map<String, PermissionAttachment> attachmentMap;

    static {
        attachmentMap = new HashMap<>();
    }

    private GamePlayerData playerData;
    private boolean adminMode;
    private DateFormat dateFormat;
    private CustomObjective customObjective;

    public GameHeartGamesPlayer(CraftServer server, EntityPlayer entity) {
        super(server, entity);

        System.out.println(getName());
        playerData = (GamePlayerData) DataManager.loadFromRedis(getName());
        if (playerData == null) {
            playerData = GamePlayerData.DEFAULT_ACCOUNT.clone();
            playerData.setName(getName());
            playerData.setFirstJoin(System.currentTimeMillis());
            playerData.setLastJoin(System.currentTimeMillis());
        }

        dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        loadPermission();
    }

    @Override
    public void loadPermission() {
        PermissionAttachment permissionAttachment = this.addAttachment(GameAPI.getAPI());
        attachmentMap.put(getName(), permissionAttachment);

        for (String perm : getMainGroup().getAllPerms()) {
            permissionAttachment.setPermission(perm, true);
            System.out.println("permission of group: " + perm);
        }

        for (Group group : getPlayerData().getRealSubGroup()) {
            for (String perm : group.getAllPerms()) {
                permissionAttachment.setPermission(perm, true);
            }
        }

        for (String perm : getPlayerData().getPermissions()) {
            permissionAttachment.setPermission(perm, true);
            System.out.println("own perm: " + perm);
        }
    }

    @Override
    public void unloadPermission() {
        PermissionAttachment permissionAttachment = getAttachmentMap().get(getName());

        for (String perm : getMainGroup().getAllPerms()) {
            permissionAttachment.unsetPermission(perm);
        }

        for (Group group : getPlayerData().getRealSubGroup()) {
            for (String perm : group.getAllPerms()) {
                permissionAttachment.unsetPermission(perm);
            }
        }

        for (String perm : getPlayerData().getPermissions()) {
            permissionAttachment.unsetPermission(perm);
        }
    }

    @Override
    public void feed() {
        this.setFoodLevel(20);
        this.setSaturation(10);
    }

    @Override
    public int getPing() {
        return getHandle().ping;
    }

    @Override
    public boolean hasAdminMode() {
        return adminMode;
    }

    @Override
    public void setAdminMode(boolean b) {
        adminMode = b;
    }

    @Override
    public void heal() {
        this.setFireTicks(0);
        this.feed();
        this.setHealth(this.getMaxHealth());
    }

    @Override
    public void playSound(Sound sound) {
        this.playSound(getLocation(), sound, 0.2f, 1f);
    }

    @Override
    public void saveGameData() {
        DataManager.sendDataToRedis(playerData);
    }

    @Override
    public void sendPlayer(String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);

        sendPluginMessage(GameAPI.getAPI(), "BungeeCord", out.toByteArray());
    }

    @Override
    public PlayerData getPlayerData() {
        return playerData;
    }

    @Override
    public SaveTag getSaveTag() {
        return SaveTag.getDefaultSaveTag();
    }

    @Override
    public Group getMainGroup() {
        return PermissionManager.getGroup(getPlayerData().getMainRank().name());
    }

    @Override
    public String getFirstJoin() {
        return dateFormat.format(new Date(getPlayerData().getFirstJoin()));
    }

    @Override
    public String getLastJoin() {
        return TimeUnit.MILLIS_SECOND.toFrench(System.currentTimeMillis() - getPlayerData().getLastJoin());
    }

    @Override
    public void setOp(boolean value) {}

    @Override
    public boolean isOp() {
        return false;
    }

    @Override
    public boolean hasPermission(String perm) {
        if (getPlayerData().hasPermission(perm))
            return true;

        return super.hasPermission(perm);
    }

    @Override
    public void unloadData() {
        if (getGameScoreboard() != null)
            getGameScoreboard().delete();

        unloadPermission();
    }

    @Override
    public void setPlayerData(PlayerData playerData) {
        this.playerData = (GamePlayerData) playerData;
    }

    @Override
    public void reloadPermissions() {
        unloadPermission();
        loadPermission();
    }

    @Override
    public void setGameScoreboard(CustomObjective customObjective) {
        this.customObjective = customObjective;
    }

    @Override
    public CustomObjective getGameScoreboard() {
        return this.customObjective;
    }

    @Override
    public boolean isStaff() {
        return getMainGroup().getPower() >= PermissionManager.getPower(Rank.HELPER);
    }
}
