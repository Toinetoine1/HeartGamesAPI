package fr.heartgames.heartgamesapi.players.packets;

import fr.heartgames.heartgamesapi.api.players.data.PlayerData;
import fr.heartgames.heartgamesapi.players.data.GamePlayerData;
import fr.heartgames.heartgamesapi.tech.redis.messaging.Packet;
import fr.heartgames.heartgamesapi.utils.general.GsonUtils;
import lombok.Getter;

/**
 * Created by Toinetoine1 on 16/08/2019.
 */

public class PacketPlayerData extends Packet {

    public static final String PACKET_TAG = "PLAYERINFOPACKET";

    @Getter
    private PlayerData playerData;
    @Getter
    private Type type;

    public PacketPlayerData() {
    }

    public PacketPlayerData(PlayerData playerData, Type type) {
        this.playerData = playerData;
        this.type = type;
    }

    @Override
    public void read(String[] data) {
        this.playerData = GsonUtils.getGson().fromJson(data[0], GamePlayerData.class);
        this.type = Type.valueOf(data[1]);
    }

    @Override
    public String[] write() {
        return new String[]{GsonUtils.getGson().toJson(this.playerData), this.type.name()};
    }

    public enum Type{
        RANK,
        SUBGROUP,
        ALL,
        PERM

    }
}
