package fr.heartgames.heartgamesapi.players.data;

import com.mongodb.client.MongoCollection;
import fr.heartgames.heartgamesapi.api.players.data.PlayerData;
import fr.heartgames.heartgamesapi.permissions.PermissionManager;
import fr.heartgames.heartgamesapi.permissions.Rank;
import fr.heartgames.heartgamesapi.permissions.groups.Group;
import fr.heartgames.heartgamesapi.tech.DataManager;
import fr.heartgames.heartgamesapi.tech.mongodb.MongoFactory;
import org.bson.Document;
import org.bson.codecs.pojo.annotations.BsonCreator;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Created by Toinetoine1 on 04/08/2019.
 */

public class GamePlayerData implements PlayerData, Cloneable {

    public static GamePlayerData DEFAULT_ACCOUNT = new GamePlayerData("N/A", true, "null", Rank.NULL,  0, -1, -1, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());

    private String name;
    private boolean onlineMode;
    private String password;
    private Rank mainRank;
    private int heartcoins;
    private long firstJoin;
    private long lastJoin;
    private List<String> permissions;
    private List<String> subGroup;
    private List<String> knownIp;

    public GamePlayerData() {}

    @BsonCreator
    public GamePlayerData(@BsonProperty("name") String name,
                          @BsonProperty("onlineMode") boolean onlineMode,
                          @BsonProperty("password") String password,
                          @BsonProperty("mainRank") Rank mainRank,
                          @BsonProperty("heartcoins") int heartcoins,
                          @BsonProperty("firstJoin") long firstJoin,
                          @BsonProperty("lastJoin") long lastJoin,
                          @BsonProperty("permissions") List<String> permissions,
                          @BsonProperty("subGroup") List<String> subGroup,
                          @BsonProperty("knownIp") List<String> knownIp) {
        this.name = name;
        this.onlineMode = onlineMode;
        this.password = password;
        this.mainRank = mainRank;
        this.heartcoins = heartcoins;
        this.firstJoin = firstJoin;
        this.lastJoin = lastJoin;
        this.permissions = permissions;
        this.subGroup = subGroup;
        this.knownIp = knownIp;
    }

    @Override
    public void addHeartcoins(int heartcoins) {
        this.heartcoins += heartcoins;
    }

    @Override
    public int getHeartcoins() {
        return this.heartcoins;
    }

    @Override
    public void removeHeartcoins(int heartcoins) {
        int result = this.heartcoins -= heartcoins;
        if (result < 0)
            this.heartcoins = 0;
    }

    @Override
    public long getFirstJoin() {
        return this.firstJoin;
    }

    @Override
    public void setFirstJoin(long firstJoin) {
        this.firstJoin = firstJoin;
    }

    @Override
    public long getLastJoin() {
        return this.lastJoin;
    }

    @Override
    public void setLastJoin(long lastJoin) {
        this.lastJoin = lastJoin;
    }

    @Override
    public List<String> getPermissions() {
        if (permissions == null)
            permissions = new ArrayList<>();

        return permissions;
    }

    @Override
    public boolean hasPermission(String perm) {
        if (getMainRank() == Rank.ADMIN)
            return true;
        Group mainGroup = PermissionManager.getGroup(getMainRank().name());

        if (mainGroup.hasPermission(perm))
            return true;
        if (mainGroup.hasPermission("*"))
            return true;
        if (getPermissions().contains(perm))
            return true;
        if(getPermissions().contains("*"))
            return true;
        if(this.getRealSubGroup().stream().anyMatch(group -> group.hasPermission(perm)))
            return true;
        if(this.getRealSubGroup().stream().anyMatch(group -> group.hasPermission("*")))
            return true;

        return false;
    }

    @Override @BsonIgnore
    public List<Group> getRealSubGroup() {
        if (subGroup == null)
            subGroup = new ArrayList<>();

        List<Group> groups = new ArrayList<>();
        for (String str : this.subGroup) {
            groups.add(PermissionManager.getGroup(str));
        }

        return groups;
    }

    @Override
    public List<String> getSubGroup() {
        if (subGroup == null)
            subGroup = new ArrayList<>();

        return subGroup;
    }

    @Override
    public boolean addSubGroup(Rank rank) {
        Group mainGroup = PermissionManager.getGroup(getMainRank().name());
        Group subGroup = PermissionManager.getGroup(rank.name());

        if (mainGroup == subGroup)
            return false;
        if (mainGroup.getPower() < subGroup.getPower())
            return false;

        if (getSubGroup().contains(subGroup.getLinkedTo().name()))
            return true;

        getSubGroup().add(subGroup.getLinkedTo().name());
        return true;
    }

    @Override
    public boolean removeSubGroup(Rank rank) {
        Group subGroup = PermissionManager.getGroup(rank.name());

        if (!this.getRealSubGroup().contains(subGroup))
            return false;

        getSubGroup().remove(subGroup.getLinkedTo().name());
        return true;
    }

    @Override
    public boolean hasSubGroup(Rank rank) {
        return this.getRealSubGroup().contains(PermissionManager.getGroup(rank));
    }

    @Override
    public void saveGameData() {
        DataManager.sendDataToRedis(this);
    }

    @Override
    public List<String> getKnownIp() {
        if(knownIp == null)
            this.knownIp = new ArrayList<>();

        return this.knownIp;
    }

    @Override @BsonIgnore
    public List<String> getKnownNames() {
        Set<String> names = new HashSet<>();

        MongoCollection<PlayerData> collection = MongoFactory.getInstance().getPlayerCollection();
        for(String ip : getKnownIp()){
            Bson filter = new Document("knownIp", ip);
            collection.find(filter).forEach((Consumer<? super PlayerData>) data -> names.add(data.getName()));
        }

        return new ArrayList<>(names);
    }

    @Override
    public Rank getMainRank() {
        return this.mainRank;
    }

    @Override
    public void setMainRank(Rank rank) {
        this.mainRank = rank;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean isOnlineMode() {
        return this.onlineMode;
    }

    @Override
    public void setPassword(String pass) {
        this.password = pass;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public boolean hasPassword() {
        return password != null && !password.equals("null");
    }

    @Override
    public void setOnlineMode(boolean b) {
        this.onlineMode = b;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public GamePlayerData clone() {
        try {
            return (GamePlayerData) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
