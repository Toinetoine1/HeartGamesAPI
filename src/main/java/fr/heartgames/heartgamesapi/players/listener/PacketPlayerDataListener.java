package fr.heartgames.heartgamesapi.players.listener;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.api.players.data.PlayerData;
import fr.heartgames.heartgamesapi.players.packets.PacketPlayerData;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketHandler;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Toinetoine1 on 16/08/2019.
 */

public class PacketPlayerDataListener implements PacketListener {

    @PacketHandler
    public void onReceive(PacketPlayerData packet) {
        PlayerData playerData = packet.getPlayerData();
        Player player = Bukkit.getPlayer(playerData.getName());

        if (player != null) {
            HeartGamesPlayer heartGamesPlayer = (HeartGamesPlayer) player;

            switch (packet.getType()) {
                case RANK:
                    heartGamesPlayer.unloadPermission();

                    System.out.println(heartGamesPlayer.getPlayerData().getMainRank() + " > " + packet.getPlayerData().getMainRank());

                    heartGamesPlayer.getPlayerData().setMainRank(packet.getPlayerData().getMainRank());
                    heartGamesPlayer.loadPermission();
                    break;
                case SUBGROUP:
                    heartGamesPlayer.unloadPermission();

                    System.out.println("Subgroup data packet: "+heartGamesPlayer.getName());
                    heartGamesPlayer.getPlayerData().getSubGroup().clear();
                    heartGamesPlayer.getPlayerData().getSubGroup().addAll(packet.getPlayerData().getSubGroup());
                    heartGamesPlayer.loadPermission();

                    break;
                case ALL:
                    heartGamesPlayer.unloadPermission();

                    heartGamesPlayer.setPlayerData(playerData);

                    heartGamesPlayer.loadPermission();
                    System.out.println("new player data from packet");
                    break;
                case PERM:
                    heartGamesPlayer.unloadPermission();

                    heartGamesPlayer.getPlayerData().getPermissions().clear();
                    heartGamesPlayer.getPlayerData().getPermissions().addAll(playerData.getPermissions());

                    heartGamesPlayer.loadPermission();
                    break;
            }

        }
    }

}
