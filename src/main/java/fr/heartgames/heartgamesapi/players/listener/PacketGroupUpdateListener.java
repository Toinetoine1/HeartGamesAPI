package fr.heartgames.heartgamesapi.players.listener;

import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.permissions.PermissionManager;
import fr.heartgames.heartgamesapi.permissions.Rank;
import fr.heartgames.heartgamesapi.permissions.packets.PacketGroupUpdate;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketHandler;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketListener;

/**
 * Created by Toinetoine1 on 26/09/2019.
 */

public class PacketGroupUpdateListener implements PacketListener {

    @PacketHandler
    public void onReceive(PacketGroupUpdate packet) {

        switch (packet.getType()) {
            case ADD:

                PermissionManager.getGroup(Rank.valueOf(packet.getRankName())).getPermissions().add(packet.getPerm());

                for (HeartGamesPlayer player : GameAPI.getAPI().getOnlinePlayers()) {
                    player.reloadPermissions();
                }
                break;
            case REMOVE:
                PermissionManager.getGroup(Rank.valueOf(packet.getRankName())).getPermissions().remove(packet.getPerm());

                for (HeartGamesPlayer player : GameAPI.getAPI().getOnlinePlayers()) {
                    player.reloadPermissions();
                }
                break;
            case RELOAD:
                for (HeartGamesPlayer player : GameAPI.getAPI().getOnlinePlayers()) {
                    player.unloadPermission();
                }

                PermissionManager.getInstance().reload();

                for (HeartGamesPlayer player : GameAPI.getAPI().getOnlinePlayers()) {
                    player.loadPermission();
                }
                break;
        }
    }

}
