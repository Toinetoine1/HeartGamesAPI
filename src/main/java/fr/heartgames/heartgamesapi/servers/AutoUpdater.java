package fr.heartgames.heartgamesapi.servers;

import fr.heartgames.heartgamesapi.api.GameAPI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.io.IOException;

public class AutoUpdater {

    private Plugin plugin;
    private String urlDownload;

    private boolean autoShutdown;

    public AutoUpdater(Plugin plugin, String urlDownload) {
        this(plugin, urlDownload, true);
    }

    public AutoUpdater(Plugin plugin, String urlDownload,
                      boolean autoShutdown) {
        this.plugin = plugin;
        this.urlDownload = urlDownload;
        this.autoShutdown = autoShutdown;
        start();
    }

    private void start() {
        Thread th = new Thread(this::startDownload);
        th.setName("Update Check");
        th.start();
    }

    private void startDownload() {
        try {
            /*HttpURLConnection download = (HttpURLConnection) new URL(this.urlDownload).openConnection();
            download.addRequestProperty("User-Agent", "Mozilla/4.76");

            BufferedInputStream in = null;
            FileOutputStream fout = null;
            try {
                plugin.getLogger().info("Trying to download from: " + download.toString());
                in = new BufferedInputStream(download.getInputStream());
                fout = new FileOutputStream("plugins" + System.getProperty("file.separator") + plugin.getName() + ".jar");

                final byte[] data = new byte[1024];
                int count;
                while ((count = in.read(data, 0, 1024)) != -1) {
                    fout.write(data, 0, count);
                }
            }catch (Exception e){
                plugin.getLogger().info("Impossible d'obtenir la version..");
                e.printStackTrace();
                return;
            } finally {
                if (in != null) {
                    in.close();
                }
                if (fout != null) {
                    fout.close();
                }
            }*/
            System.out.println("command: "+GameAPI.getAPI().getDataFolder().getAbsolutePath()+"../");
            Runtime.getRuntime().exec("cp /repository/HeartGamesAPI.jar "+GameAPI.getAPI().getDataFolder().getAbsolutePath()+"/../");

            plugin.getLogger().info("Téléchargement complété !");
            if(this.autoShutdown){
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop");
                StringBuilder path = new StringBuilder();
                String[] splited = GameAPI.getAPI().getDataFolder().getAbsolutePath().split("/");
                String word = "plugins";

                for(int i = 1; i < splited.length; i++){
                    if(splited[i].equalsIgnoreCase(word)){
                        break;
                    }
                    path.append("/").append(splited[i]);
                    System.out.println("i="+i+" path: "+path.toString());
                }
                String finalPath = path.substring(0, path.length()) + "/start.sh";

                System.out.println("Pathh: "+ finalPath);
                try {
                    Process p = Runtime.getRuntime().exec(finalPath);
                    p.waitFor();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            plugin.getLogger().info("Impossible d'obtenir la version..");
            e.printStackTrace();
        }
    }

}