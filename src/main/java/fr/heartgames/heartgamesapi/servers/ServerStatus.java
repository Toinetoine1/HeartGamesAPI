package fr.heartgames.heartgamesapi.servers;

/**
 * Created by Toinetoine1 on 12/08/2019.
 */

public enum ServerStatus {

    ALLOW,
    INGAME,
    VIP,
    DISALLOW
}
