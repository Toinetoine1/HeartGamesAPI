package fr.heartgames.heartgamesapi.servers.listeners;

import fr.heartgames.heartgamesapi.servers.ServerManager;
import fr.heartgames.heartgamesapi.servers.packets.PacketServerOnlinePlayers;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketHandler;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketListener;

/**
 * Created by Toinetoine1 on 12/10/2019.
 */

public class PacketOnlinePlayersListener implements PacketListener {

    @PacketHandler
    public void onReceive(PacketServerOnlinePlayers.Response packet){
        ServerManager.getOnlinePlayers().put(packet.getServerName(), packet.getCount());
    }

}
