package fr.heartgames.heartgamesapi.servers.listeners;

import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.servers.ServerManager;
import fr.heartgames.heartgamesapi.servers.packets.PacketServerName;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketHandler;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketListener;
import org.bukkit.Bukkit;


/**
 * Created by Toinetoine1 on 29/10/2019.
 */

public class PacketServerNameListener implements PacketListener {

    public static String serverName = "null";

    @PacketHandler
    public void onReceive(PacketServerName.Response packet) {
        if (packet.getPort() == Bukkit.getServer().getPort()) {
            if (GameAPI.getAPI().getGameServer() != null){
                GameAPI.getAPI().getGameServer().setServerName(packet.getServerName());

                ServerManager.publishServer(GameAPI.getAPI().getGameServer());
            }
        }
    }

}
