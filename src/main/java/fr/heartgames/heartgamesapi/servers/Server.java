package fr.heartgames.heartgamesapi.servers;

import lombok.Setter;

/**
 * Created by Toinetoine1 on 12/08/2019.
 */


public class Server {

    @Setter
    private String serverName;
    private ServerType serverType;
    private ServerStatus serverStatus;

    public Server(ServerType serverType, ServerStatus serverStatus) {
        this.serverType = serverType;
        this.serverStatus = serverStatus;
    }

    public ServerType getServerType() {
        return serverType;
    }

    public ServerStatus getServerStatus() {
        return serverStatus;
    }

    public String getServerName() {
        return serverName;
    }
}
