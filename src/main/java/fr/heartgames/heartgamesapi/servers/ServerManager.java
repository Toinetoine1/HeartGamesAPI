package fr.heartgames.heartgamesapi.servers;

import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.jsonconfig.ServerConfig;
import fr.heartgames.heartgamesapi.servers.packets.PacketServerName;
import fr.heartgames.heartgamesapi.tech.redis.datastorage.RedisDataHandler;
import fr.heartgames.heartgamesapi.tech.redis.messaging.Redis;
import fr.heartgames.heartgamesapi.utils.Utils;
import lombok.Getter;
import org.bukkit.Bukkit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Toinetoine1 on 12/08/2019.
 */

public class ServerManager {

    private static final String SERVER_TAG = "SERVER";

    @Getter
    private static Map<String, Integer> onlinePlayers = new HashMap<>();

    public static Server loadServer(ServerConfig config){
        Redis.publishOnMainChannel(new PacketServerName.Request(getPort()));

        return new Server(config.getServerType(), config.getServerStatus());
    }

    public static void publishServer(Server server){
        String redisKey = SERVER_TAG+":"+server.getServerName().concat(":").concat(GameAPI.getAPI().getRunType().name());

        Map<String, String> map = new HashMap<>();
        map.put("ServerName", server.getServerName());
        map.put("Online", "true");
        map.put("Status", server.getServerStatus().name());
        map.put("Type", server.getServerType().name());

        if (!RedisDataHandler.containsKey(redisKey)) {
            Utils.info("&3No server found with this data.. Creating new server in Redis !");
        } else {
            if(RedisDataHandler.getField(redisKey, "Online").equalsIgnoreCase("true")){
                Utils.severe("&cA server is running with this data !");
                Bukkit.shutdown();
                return;
            }

            RedisDataHandler.deleteKey(redisKey);
        }

        RedisDataHandler.addMap(redisKey, map);
    }

    public static void shutdown(Server server){
        String redisKey = SERVER_TAG+":"+server.getServerName().concat(":").concat(GameAPI.getAPI().getRunType().name());

        RedisDataHandler.updateMapValue(redisKey, "Online", "false");
    }

    public static int getOnlinePlayers(String serverName){
        return onlinePlayers.getOrDefault(serverName, 0);
    }

    private static int getPort(){
        int port = -1;

        try {
            BufferedReader is = new BufferedReader(new FileReader("server.properties"));
            Properties props = new Properties();
            props.load(is);
            is.close();
            port = Integer.parseInt(props.getProperty("server-port"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return port;
    }
}
