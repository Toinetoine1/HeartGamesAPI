package fr.heartgames.heartgamesapi.servers.packets;

import fr.heartgames.heartgamesapi.api.players.data.PlayerData;
import fr.heartgames.heartgamesapi.players.data.GamePlayerData;
import fr.heartgames.heartgamesapi.servers.Server;
import fr.heartgames.heartgamesapi.tech.redis.messaging.Packet;
import fr.heartgames.heartgamesapi.utils.general.GsonUtils;
import lombok.Getter;

/**
 * Created by Toinetoine1 on 19/08/2019.
 */


public abstract class PacketPlayerJoin extends Packet {

    public PacketPlayerJoin() {
    }

    public static class Request extends PacketPlayerJoin {
        public static final String PACKET_TAG = "PACKETJOINPLAYER_REQUEST";

        @Getter
        private PlayerData playerData;
        @Getter
        private Server server;

        public Request() {
        }

        public Request(PlayerData playerData, Server server) {
            this.playerData = playerData;
            this.server = server;
        }

        @Override
        public void read(String[] data) {
            this.playerData = GsonUtils.getGson().fromJson(data[0], GamePlayerData.class);
            this.server = GsonUtils.getGson().fromJson(data[1], Server.class);
        }

        @Override
        public String[] write() {
            return new String[]{GsonUtils.getGson().toJson(playerData), GsonUtils.getGson().toJson(server)};
        }
    }

    public static class Response extends PacketPlayerJoin {
        public static final String PACKET_TAG = "PACKETJOINPLAYER_RESPONSE";

        @Getter
        private PlayerData playerData;
        @Getter
        private Server server;
        @Getter
        private Response.Type type;
        @Getter
        private String message;

        public Response() {
        }

        public Response(PlayerData playerData, Server server, Type type, String message) {
            this.playerData = playerData;
            this.server = server;
            this.type = type;
            this.message = message;
        }

        @Override
        public void read(String[] data) {
            this.playerData = GsonUtils.getGson().fromJson(data[0], GamePlayerData.class);
            this.server = GsonUtils.getGson().fromJson(data[1], Server.class);
            this.type = Type.valueOf(data[2]);
            this.message = data[3];
        }

        @Override
        public String[] write() {
            return new String[]{GsonUtils.getGson().toJson(playerData), GsonUtils.getGson().toJson(server), type.name(), message};
        }

        public enum Type {
            ALLOW,
            DISALOW
        }
    }
}
