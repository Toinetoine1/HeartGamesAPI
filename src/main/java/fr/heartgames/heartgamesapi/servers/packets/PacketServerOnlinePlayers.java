package fr.heartgames.heartgamesapi.servers.packets;

import fr.heartgames.heartgamesapi.tech.redis.messaging.Packet;
import lombok.Getter;

/**
 * Created by Toinetoine1 on 12/10/2019.
 */


public abstract class PacketServerOnlinePlayers extends Packet {

    public PacketServerOnlinePlayers() {
    }

    public static class Request extends PacketServerOnlinePlayers {
        public static final String PACKET_TAG = "PACKETSERVERONLINEPLAYERS_REQUEST";

        @Getter
        private String serverName;

        public Request() {
        }

        public Request(String serverName) {
            this.serverName = serverName;
        }

        @Override
        public void read(String[] data) {
            this.serverName = data[0];
        }

        @Override
        public String[] write() {
            return new String[]{serverName};
        }
    }

    public static class Response extends PacketServerOnlinePlayers {

        public static final String PACKET_TAG = "PACKETSERVERONLINEPLAYERS_RESPONSE";

        @Getter
        private String serverName;
        @Getter
        private int count;

        public Response() {
        }

        public Response(String serverName, int count) {
            this.serverName = serverName;
            this.count = count;
        }

        @Override
        public void read(String[] data) {
            this.serverName = data[0];
            this.count = Integer.parseInt(data[1]);
        }

        @Override
        public String[] write() {
            return new String[]{serverName, count+""};
        }
    }

}
