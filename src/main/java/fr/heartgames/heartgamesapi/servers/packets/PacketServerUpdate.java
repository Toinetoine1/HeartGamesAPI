package fr.heartgames.heartgamesapi.servers.packets;

import fr.heartgames.heartgamesapi.tech.redis.messaging.Packet;
import lombok.Getter;

/**
 * Created by Toinetoine1 on 19/10/2019.
 */

public class PacketServerUpdate extends Packet {

    public static final String PACKET_TAG = "PACKETSERVERUPDATE";

    @Getter
    private String serverName;

    public PacketServerUpdate() {
    }

    public PacketServerUpdate(String serverName) {
        this.serverName = serverName;
    }

    @Override
    public void read(String[] data) {
        this.serverName = data[0];
    }

    @Override
    public String[] write() {
        return new String[]{serverName};
    }

}
