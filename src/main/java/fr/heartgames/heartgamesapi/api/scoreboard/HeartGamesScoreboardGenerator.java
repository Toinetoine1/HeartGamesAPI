package fr.heartgames.heartgamesapi.api.scoreboard;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;

/**
 * Created by Toinetoine1 on 19/08/2019.
 */

public abstract class HeartGamesScoreboardGenerator {

    protected CustomObjective objective;
    protected HeartGamesPlayer player;

    public abstract void generate();

    public CustomObjective getObjective(){
        return objective;
    }

    public HeartGamesPlayer getPlayer() {
        return player;
    }
}
