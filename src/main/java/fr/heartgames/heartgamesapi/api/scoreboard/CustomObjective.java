package fr.heartgames.heartgamesapi.api.scoreboard;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;

import java.util.Collection;
import java.util.List;

/**
 * Created by Toinetoine1 on 19/08/2019.
 */

public interface CustomObjective {

    HeartGamesPlayer getPlayer();

    void delete();

    void generate();

    String getTitle();

    void updateTitle(String title);

    List<String> getLines();

    void updateLines(String... lines);

    void updateLines(Collection<String> lines);

    String getLine(int line);

    boolean isDeleted();

    void setGenerator(HeartGamesScoreboardGenerator generator);
}

