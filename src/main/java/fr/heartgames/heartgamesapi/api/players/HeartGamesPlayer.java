package fr.heartgames.heartgamesapi.api.players;

import fr.heartgames.heartgamesapi.api.players.data.PlayerData;
import fr.heartgames.heartgamesapi.api.scoreboard.CustomObjective;
import fr.heartgames.heartgamesapi.permissions.groups.Group;
import fr.heartgames.heartgamesapi.tech.redis.SaveTag;
import lombok.Getter;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Created by Toinetoine1 on 02/08/2019.
 */

public interface HeartGamesPlayer extends Player {

    public static enum GamePermission {
        PLAYER(null),
        VIP("heartgames.vip"),
        HELPER("heartgames.helper"),
        DEV("heartgames.dev"),
        MODERATOR("heartgames.modo"),
        BMODERATOR("heartgames.modo+"),
        RESP("heartgames.resp"),
        ADMIN("heartgames.admin");

        @Getter
        private final String permission;

        private GamePermission(String permission) {
            this.permission = permission;
        }
    }

    public void feed();

    public String getName();

    public Object getHandle();

    public int getPing();

    public boolean hasAdminMode();

    public void setAdminMode(boolean b);

    public void heal();

    public void playSound(Sound sound);

    public void saveGameData();

    public void sendPlayer(String server);

    public PlayerData getPlayerData();

    public SaveTag getSaveTag();

    public Group getMainGroup();

    public String getFirstJoin();

    public String getLastJoin();

    public boolean hasPermission(String perm);

    public void unloadData();

    public void setPlayerData(PlayerData playerData);

    public void loadPermission();

    public void unloadPermission();

    public void reloadPermissions();

    public void setGameScoreboard(CustomObjective customObjective);

    public CustomObjective getGameScoreboard();

    public boolean isStaff();

}
