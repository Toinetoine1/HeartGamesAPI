package fr.heartgames.heartgamesapi.permissions.groups;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import fr.heartgames.heartgamesapi.permissions.Rank;
import fr.heartgames.heartgamesapi.utils.general.GsonUtils;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Toinetoine1 on 11/08/2019.
 */

public class Group {

    private static Type listType = new TypeToken<List<String>>(){}.getType();
    private int power;
    private List<String> inheritance;
    private List<String> permissions;
    @Setter @Getter
    private transient List<String> allPerms;
    @Setter @Getter
    private transient Rank linkedTo;

    public Group() {
    }

    public Group(int power, List<String> inheritance, List<String> permissions) {
        this.power = power;
        this.inheritance = inheritance;
        this.permissions = permissions;
    }

    public Group(JsonObject jsonObject){
        this.power = jsonObject.get("power").getAsInt();
        this.inheritance = GsonUtils.getGson().fromJson(jsonObject.get("inheritance"), listType);
        this.permissions = GsonUtils.getGson().fromJson(jsonObject.get("permissions"), listType);
    }


    public int getPower() {
        return power;
    }

    public List<String> getInheritance() {
        return inheritance;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public boolean hasPermission(String perm){
        return allPerms.contains(perm);
    }

    /*public DBObject getDBObject(){
        BasicDBObject dbObject = new BasicDBObject();
        dbObject.put("power", this.power);
        dbObject.put("inheritance", inheritance);
        dbObject.put("permissions", permissions);
        return dbObject;
    }*/

}
