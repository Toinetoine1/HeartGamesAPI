package fr.heartgames.heartgamesapi.permissions;

/**
 * Created by Toinetoine1 on 05/08/2019.
 */

public enum Rank {

    NULL(0, "Joueur", "§7"),
    VIP(10, "VIP", "§eVIP "),
    VIPPLUS(20, "VIP+", "§6VIP+ "),
    HERO(20, "Hero", "§dHero "),
    LEGEND(25, "Legend", "§5Legend "),
    HELPER(40, "Helper", "§bHelper "),
    DEVELOPPEUR(45,"Développeur","§2Développeur "),
    BUILDEUR(50,"Buildeur","§aBuilder "),
    MODERATEUR(75, "Modérateur", "§9Modérateur "),
    SUPERMODO(80, "SuperModo", "§6Super§9Modo "),
    RESP(95, "Responsable", "§cResponsable "),
    ADMIN(100, "Admin", "§4Admin ");

    private int power;
    private String name;
    private String formattedName;

    Rank(int power, String name, String formattedName) {
        this.power = power;
        this.name = name;
        this.formattedName = formattedName;
    }

    @Deprecated
    public int getPower() {
        return power;
    }

    public String getName() {
        return name;
    }

    public String getFormattedName() {
        return formattedName;
    }
}
