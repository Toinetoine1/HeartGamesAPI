package fr.heartgames.heartgamesapi.tech.redis.connection;

import fr.heartgames.heartgamesapi.jsonconfig.RedisCredentials;
import fr.heartgames.heartgamesapi.tech.redis.RedisFactory;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;

/**
 * Created by Toinetoine1 on 03/08/2019.
 */


public class RedisAccess {

    private static RedisAccess INSTANCE;
    private RedissonClient redissonClient;

    private RedisAccess(RedisCredentials redisCredentials){
        INSTANCE = this;
        this.redissonClient = initRedisson(redisCredentials);
        redissonClient.getNodesGroup().addConnectionListener(new RedisConnectionListener());
    }

    public static void init(RedisCredentials redisCredentials){
        RedisAccess redisAccess = new RedisAccess(redisCredentials);
        RedisFactory.getInstance().setRedisAccess(redisAccess);
    }

    public static void close(){
        RedisAccess.INSTANCE.getRedissonClient().shutdown();
    }

    private RedissonClient initRedisson(RedisCredentials redisCredentials){
        final Config config = new Config();

        config.setCodec(new JsonJacksonCodec());
        config.setThreads(8);
        config.setNettyThreads(8);
        config.useSingleServer()
                .setAddress(redisCredentials.getIp()+":"+redisCredentials.getPort())
                .setPassword(redisCredentials.getPassword())
                .setDatabase(1)
                .setClientName(redisCredentials.getClientName());

        return Redisson.create(config);
    }

    public RedissonClient getRedissonClient() {
        return redissonClient;
    }
}
