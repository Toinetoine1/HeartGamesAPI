package fr.heartgames.heartgamesapi.tech.redis;

import fr.heartgames.heartgamesapi.loader.ModularLoader;
import lombok.Getter;

/**
 * Created by Toinetoine1 on 04/08/2019.
 */

public enum SaveTag {
    PROD("PROD"),
    DEV("DEV");

    @Getter
    String redisTag;

    SaveTag(String redisTag){
        this.redisTag = redisTag;
    }

    public static SaveTag getByRedisTag(String redisTag) {
        for (SaveTag saveTag : values()) {
            if (saveTag.getRedisTag().equals(redisTag)) {
                return saveTag;
            }
        }
        return null;
    }

    public static SaveTag getDefaultSaveTag() {
        return ModularLoader.dev ? SaveTag.DEV : SaveTag.PROD;
    }
}
