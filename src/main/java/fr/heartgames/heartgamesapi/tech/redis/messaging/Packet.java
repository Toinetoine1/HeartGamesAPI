package fr.heartgames.heartgamesapi.tech.redis.messaging;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;

public abstract class Packet {

    @Setter
    @Getter
    private Target target = Target.ALL;

    public abstract void read(String[] data);

    public abstract String[] write();

    @Override
    public String toString() {
        return "Packet{" + Arrays.toString(this.write()) + '}';
    }
}
