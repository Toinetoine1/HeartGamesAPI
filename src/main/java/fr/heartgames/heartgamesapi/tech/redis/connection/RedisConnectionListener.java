package fr.heartgames.heartgamesapi.tech.redis.connection;

import org.redisson.connection.ConnectionListener;

import java.net.InetSocketAddress;

/**
 * Created by Toinetoine1 on 19/08/2019.
 */

public class RedisConnectionListener implements ConnectionListener {

    @Override
    public void onConnect(InetSocketAddress address) {
        System.out.println("Connecting to: "+address.getHostString());
    }

    @Override
    public void onDisconnect(InetSocketAddress address) {
        System.out.println("Disconnecting to: "+address.getHostString());
    }
}
