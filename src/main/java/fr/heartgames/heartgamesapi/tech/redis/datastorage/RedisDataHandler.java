package fr.heartgames.heartgamesapi.tech.redis.datastorage;

import fr.heartgames.heartgamesapi.tech.redis.RedisFactory;
import fr.heartgames.heartgamesapi.tech.redis.SaveTag;
import org.redisson.api.RBucket;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;

import java.util.Map;

public class RedisDataHandler {

    private static RedisFactory factory = RedisFactory.getInstance();
    private static RedissonClient redissonClient = factory.getRedisClient();

    public static void setUserData(String name, String value){
        String key = "PLAYER:"+name.concat(":".concat(SaveTag.getDefaultSaveTag().getRedisTag()));
        System.out.println("set userdata rediskey: "+key);

        RBucket<String> bucket = redissonClient.getBucket(key);
        bucket.set(value);
    }

    public static String getUserData(String name){
        String key = "PLAYER:"+name.concat(":".concat(SaveTag.getDefaultSaveTag().getRedisTag()));
        System.out.println("get userdata rediskey: "+key);
        RBucket<String> bucket = redissonClient.getBucket(key);
        return bucket.get();
    }

    public static boolean containsUser(String saveTag, String name) {
        return saveTag != null && name != null && containsKey("PLAYER:"+name.concat(":".concat(SaveTag.getDefaultSaveTag().getRedisTag())));
    }

    public static void deleteUser(String name) {
        String key = "PLAYER:"+name.concat(":".concat(SaveTag.getDefaultSaveTag().getRedisTag()));
        deleteKey(key);
    }

    public static boolean containsKey(String key){
        return redissonClient.getBucket(key).isExists();
    }

    public static String getString(String key) {
        RBucket<String> rBucket = redissonClient.getBucket(key);
        return rBucket.get();
    }

    public static void setString(String key, String value) {
        RBucket<String> rBucket = redissonClient.getBucket(key);
        rBucket.set(value);
    }

    public static void deleteKey(String key){
        redissonClient.getBucket(key).delete();
    }



    /*
        Utilitaires relatives aux MAP
     */

    public static String getField(String key, String field) {
        RMap<String, String> rMap = redissonClient.getMap(key);
        return rMap.getOrDefault(field, null);
    }

    public static void addMap(String key, Map<String, String> map){
        RMap<String, String> rMap = redissonClient.getMap(key);
        rMap.putAll(map);
    }

    public static void updateMapValue(String key, String field, String value){
        RMap<String, String> rMap = redissonClient.getMap(key);
        rMap.fastPut(field, value);
    }

    public static void deleteField(String key, String... fields){
        RMap<String, String> rMap = redissonClient.getMap(key);
        rMap.fastRemove(fields);
    }
    /*
    public static void resetPlayer(UserInfo userinfo) {
        HashMap <String, String> map = new HashMap <>();
        map.put("", "");
        RedisDataHandler.addMap(userinfo.getSaveTag().getRedisTag(), userinfo.getUuid(), map);
        UserManager.removeOfflineUserInfo(userinfo.getSaveTag(), userinfo.getUuid());
        UserManager.removeOnlineUserInfo(userinfo.getSaveTag(), userinfo.getUuid());
    }*/
}
