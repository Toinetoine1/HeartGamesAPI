package fr.heartgames.heartgamesapi.tech.redis.messaging;

import fr.heartgames.heartgamesapi.tech.redis.RedisFactory;
import fr.heartgames.heartgamesapi.utils.Utils;
import org.redisson.api.RTopic;
import org.redisson.api.listener.MessageListener;

import java.util.logging.Level;
import java.util.logging.Logger;

public class PubSubManager implements MessageListener {

    /**
     * Use to publish Packet through Redis messaging service.
     *
     * @param packet to send.
     */
    void publish(String channel, Packet packet) {
        try {
            System.out.println("PUBLISHING PACKET: "+packet+" ON CHANNEL "+channel);
            RTopic<Packet> rTopic = RedisFactory.getInstance().getRedisClient().getTopic(channel);
            rTopic.publish(packet);
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "Failed to publish ", e);
        }
    }

    /*@Override
    public void onMessage(CharSequence channel, Object message) {
        if (RedisFactory.getChannelsRegistered().contains(channel.toString())) {
            if (message instanceof Packet) {
                try {
                    Packet packet = (Packet) message;
                    Redis.received(packet);
                } catch (Exception e) {
                    Utils.severe("Enable to cast the Packet..");
                }
            } else {
                Utils.severe("Impossible de recevoir ce packet !");
            }
        }
    }*/

    @Override
    public void onMessage(String channel, Object message) {
        if (RedisFactory.getChannelsRegistered().contains(channel)) {
            if (message instanceof Packet) {
                try {
                    Packet packet = (Packet) message;
                    Redis.received(packet);
                } catch (Exception e) {
                    Utils.severe("Enable to cast the Packet..");
                }
            } else {
                Utils.severe("Impossible de recevoir ce packet !");
            }
        }
    }
}
