package fr.heartgames.heartgamesapi.tech.mongodb.codec;

import fr.heartgames.heartgamesapi.permissions.Rank;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

/**
 * Created by Toinetoine1 on 11/08/2019.
 */

public class RankEnumCodec implements Codec<Rank>{

    @Override
    public Rank decode(final BsonReader reader, final DecoderContext decoderContext) {
        return Rank.valueOf(reader.readString());
    }

    @Override
    public void encode(final BsonWriter writer, final Rank value, final EncoderContext encoderContext) {
        writer.writeString(value.name());
    }

    @Override
    public Class<Rank> getEncoderClass() {
        return Rank.class;
    }
}
