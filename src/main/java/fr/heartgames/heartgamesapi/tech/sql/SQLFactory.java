package fr.heartgames.heartgamesapi.tech.sql;

import fr.heartgames.heartgamesapi.moderation.utils.SQLModerationQuery;
import fr.heartgames.heartgamesapi.utils.IQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Toinetoine1 on 22/08/2019.
 */

public class SQLFactory {

    public static void executeStatement(IQuery sql, Object... parameters) throws SQLException {
        executeStatement(sql, false, DatabaseAccess.getInstance().getConnection() ,parameters);
    }

    public static PreparedStatement executeResultStatement(IQuery sql, Connection connection, Object... parameters) {
        return executeStatement(sql, true, connection, parameters);
    }

    private static PreparedStatement executeStatement(IQuery sql, boolean result, Connection connection, Object... parameters) {
        return executeStatement(sql.getString(), result, connection, parameters);
    }

    public static PreparedStatement executeStatement(String sql, boolean result, Connection connection, Object... parameters) {
        try {
            System.out.println("request: " + sql);
            PreparedStatement statement = connection.prepareStatement(sql);

            for (int i = 0; i < parameters.length; i++) {
                Object obj = parameters[i];
                if (obj instanceof Integer) {
                    statement.setInt(i + 1, (Integer) obj);
                } else if (obj instanceof String) {
                    statement.setString(i + 1, (String) obj);
                } else if (obj instanceof Long) {
                    statement.setLong(i + 1, (Long) obj);
                } else {
                    statement.setObject(i + 1, obj);
                }
            }


            if (result) {
                return statement;
            } else {
                statement.execute();
                statement.close();
                connection.close();
            }
            return null;
        } catch (SQLException ex) {
            System.out.println("ERROR ON SQL !");
            ex.printStackTrace();
            return null;
        }
    }

}
