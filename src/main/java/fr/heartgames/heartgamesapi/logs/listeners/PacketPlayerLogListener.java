package fr.heartgames.heartgamesapi.logs.listeners;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.inventory.ClickAction;
import fr.heartgames.heartgamesapi.inventory.CustomHolder;
import fr.heartgames.heartgamesapi.inventory.Icon;
import fr.heartgames.heartgamesapi.inventory.InteractiveHolder;
import fr.heartgames.heartgamesapi.logs.Action;
import fr.heartgames.heartgamesapi.logs.Log;
import fr.heartgames.heartgamesapi.logs.packets.PacketPlayerLog;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketHandler;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketListener;
import fr.heartgames.heartgamesapi.utils.items.Item;
import fr.heartgames.heartgamesapi.utils.items.SkullItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Toinetoine1 on 22/11/2019.
 */

public class PacketPlayerLogListener implements PacketListener {

    private static Map<Action, Material> map = new HashMap<>();

    static {
        map.put(Action.UNKNOWN, Material.BARRIER);
        map.put(Action.COMMAND, Material.STICK);
        map.put(Action.BLOCK_PUT, Material.STONE);
        map.put(Action.BLOCK_REMOVE, Material.COBBLESTONE);
    }

    @PacketHandler
    public void onReceive(PacketPlayerLog packet) {
        Player player = Bukkit.getPlayer(packet.getPlayer());

        if (player != null) {
            List<Log> logs = packet.getLogs().stream().filter(Objects::nonNull).collect(Collectors.toList());

            System.out.println(packet.getLogs());
            Inventory inventory = new LogInventory(logs, packet.getTargeted()).getInventory();
            player.openInventory(inventory);
        }
    }

    private class LogInventory extends InteractiveHolder {

        private List<Integer> slot;
        private List<Log> logs;
        private int page;
        private int nextPageSlot;
        private int previousPageSlot;

        public LogInventory(List<Log> logs, String target) {
            super(9 * 6, "§eLogs de §c" + target);
            this.logs = logs;

            this.slot = new ArrayList<>();
            for (int i = 0; i < 9 * 6 - 2; i++) {
                slot.add(i);
            }
            this.nextPageSlot = slot.size() + 1;
            this.previousPageSlot = slot.size();
            this.page = 1;

            openNewPage(0);
/*            for (int i = 0; i < logs.size(); i++) {
                Log log = logs.get(i);
                Action action = log.getAction();

                Item item = new Item(map.get(action));
                item.setDisplayName("§3" + action.getName());
                item.setLoreLines(new ArrayList<>(Arrays.asList("", "§6Valeur: §e" + log.getValue(), "§6Date: §e" + log.getDate(), "§6Serveur: §e" + log.getServer())));

                setIcon(i, new Icon(item.asStack()));
            }*/
        }

        private void openNewPage(int slotIndex) {
            super.icons.values().forEach(icon -> icon.getClickActions().clear());

            if (logs.size() == 0) {
                return;
            }

            if (page > 1) {
                for (int i = 0; i < slot.size(); i++) {
                    this.getInventory().setItem(i, new ItemStack(Material.AIR));
                }
                this.getInventory().setItem(nextPageSlot, new ItemStack(Material.AIR));
                setIcon(nextPageSlot, new Icon(Material.AIR));
                setIcon(previousPageSlot, new Icon(SkullItem.createHeadByData("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmQ2OWUwNmU1ZGFkZmQ4NGU1ZjNkMWMyMTA2M2YyNTUzYjJmYTk0NWVlMWQ0ZDcxNTJmZGM1NDI1YmMxMmE5In19fQ==", 1, "§cPage précédente", new ArrayList<>()))
                        .addClickAction(new ClickAction() {
                            @Override
                            public void execute(HeartGamesPlayer player, ItemStack clickedItem) {
                                getInventory().setItem(previousPageSlot, new ItemStack(Material.AIR));
                                page--;
                                openNewPage(slot.size() * page - slot.size() + (page > 1 ? 1 : 0));
                            }
                        }));
            } else {
                this.getInventory().setItem(previousPageSlot, new ItemStack(Material.AIR));
            }

            for (int i = slotIndex; i < logs.size(); i++) {
                Log log = logs.get(i);
                int realSlot = (i - ((page - 1) * slot.size())) + (page == 1 ? 0 : (-1 * (page - 1)));

                if (slot.size() == realSlot) {
                    int finalI = i;
                    setIcon(nextPageSlot, new Icon(SkullItem.createHeadByData("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTliZjMyOTJlMTI2YTEwNWI1NGViYTcxM2FhMWIxNTJkNTQxYTFkODkzODgyOWM1NjM2NGQxNzhlZDIyYmYifX19", 1, "§cPage suivante", new ArrayList<>()))
                            .addClickAction(new ClickAction() {
                                @Override
                                public void execute(HeartGamesPlayer player, ItemStack clickedItem) {
                                    getInventory().setItem(nextPageSlot, new ItemStack(Material.AIR));
                                    page++;
                                    openNewPage(finalI + 1);
                                }

                            }));
                    break;
                }
                // 16 - ((2 - 1) * 15)
                Action action = log.getAction();
                Item item = new Item(map.get(action));
                item.setDisplayName("§3" + action.getName());
                item.setLoreLines(new ArrayList<>(Arrays.asList("", "§6Valeur: §e" + log.getValue(), "§6Date: §e" + log.getDate(), "§6Serveur: §e" + log.getServer())));

                setIcon(slot.get(this.page == 1 ? i : realSlot), new Icon(item.asStack()));
            }
        }
    }

}
