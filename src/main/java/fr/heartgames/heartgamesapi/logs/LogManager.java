package fr.heartgames.heartgamesapi.logs;

import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.logs.query.SQLLogsQuery;
import fr.heartgames.heartgamesapi.tech.sql.DatabaseAccess;
import fr.heartgames.heartgamesapi.tech.sql.SQLFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Toinetoine1 on 22/11/2019.
 */

public class LogManager {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy 'a' HH:mm:ss");

    public static void log(String player, Action action, String value){
        Date date = new Date();

        String currentTime = dateFormat.format(date);
        try {
            SQLFactory.executeStatement(SQLLogsQuery.INSERT_STAFF_LOGS, player, action.name(), value, currentTime, GameAPI.getAPI().getGameServer().getServerName());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<Log> getLog(String player, Action action){
        List<Log> logs = new ArrayList<>();
        try {
            Connection connection = DatabaseAccess.getInstance().getConnection();
            PreparedStatement result = SQLFactory.executeResultStatement(SQLLogsQuery.SELECT_ALL_LOGS_LIMIT_WITH_ACTION, connection, player, action.name(), 10);
            ResultSet resultSet = result.executeQuery();
            while(resultSet.next()){
                logs.add(new Log(player, action, resultSet.getString("value"), resultSet.getString("date"), resultSet.getString("server")));
            }

            resultSet.close();
            result.close();
            connection.close();
        } catch (SQLException e) {
                    e.printStackTrace();
        }
        return logs;
    }

    public static List<Log> getLog(String player){
        List<Log> logs = new ArrayList<>();
        try {
            Connection connection = DatabaseAccess.getInstance().getConnection();
            PreparedStatement result = SQLFactory.executeResultStatement(SQLLogsQuery.SELECT_ALL_LOGS, connection, player);
            ResultSet resultSet = result.executeQuery();
            while(resultSet.next()){
                logs.add(new Log(player, Action.valueOf(resultSet.getString("action")), resultSet.getString("value"), resultSet.getString("date"), resultSet.getString("server")));
            }

            resultSet.close();
            result.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return logs;
    }

}
