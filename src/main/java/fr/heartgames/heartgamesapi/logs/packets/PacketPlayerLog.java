package fr.heartgames.heartgamesapi.logs.packets;

import com.google.gson.reflect.TypeToken;
import fr.heartgames.heartgamesapi.logs.Log;
import fr.heartgames.heartgamesapi.tech.redis.messaging.Packet;
import fr.heartgames.heartgamesapi.utils.general.GsonUtils;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Toinetoine1 on 22/11/2019.
 */

public class PacketPlayerLog extends Packet {

    public static String PACKETTAG = "PACKETPLAYERLOG";
    private static Type type = new TypeToken<List<Log>>() {}.getType();

    private String player;
    private String targeted;
    private List<Log> logs;

    public PacketPlayerLog() {}

    public PacketPlayerLog(String player, String targeted, List<Log> logs) {
        this.player = player;
        this.targeted = targeted;
        this.logs = logs;
    }

    @Override
    public void read(String[] data) {
        player = data[0];
        targeted = data[1];
        logs = GsonUtils.getCompactGson().fromJson(data[2], type);
    }

    @Override
    public String[] write() {
        return new String[]{player, targeted, GsonUtils.getCompactGson().toJson(logs)};
    }

    public String getPlayer() {
        return player;
    }

    public String getTargeted() {
        return targeted;
    }

    public List<Log> getLogs() {
        return logs;
    }
}
