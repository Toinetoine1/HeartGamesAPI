package fr.heartgames.heartgamesapi.logs;

import org.bukkit.Material;

/**
 * Created by Toinetoine1 on 22/11/2019.
 */

public enum Action {

    UNKNOWN("Inconnue"),
    COMMAND("Commande"),
    BLOCK_PUT("Block cassé"),
    BLOCK_REMOVE("Block posé");

    private String name;

    Action(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
