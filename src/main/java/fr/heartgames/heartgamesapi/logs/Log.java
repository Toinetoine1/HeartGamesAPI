package fr.heartgames.heartgamesapi.logs;

import java.util.Date;

/**
 * Created by Toinetoine1 on 22/11/2019.
 */

public class Log {

    private String player;
    private Action action;
    private String value;
    private String date;
    private String server;

    public Log() {
    }

    public Log(String player, Action action, String value, String date, String server) {
        this.player = player;
        this.action = action;
        this.value = value;
        this.date = date;
        this.server = server;
    }

    public String getPlayer() {
        return player;
    }

    public Action getAction() {
        return action;
    }

    public String getValue() {
        return value;
    }

    public String getDate() {
        return date;
    }

    public String getServer() {
        return server;
    }
}
