package fr.heartgames.heartgamesapi.logs.query;

import fr.heartgames.heartgamesapi.utils.IQuery;

/**
 * Created by Toinetoine1 on 22/11/2019.
 */

public enum SQLLogsQuery implements IQuery {

    INSERT_STAFF_LOGS(
            "INSERT INTO `StaffLogs` " +
                    "(`name`, `action`, `value`, `date`, `server`) " +
                    "VALUES (?, ?, ?, ?, ?)"
    ),
    SELECT_ALL_LOGS_LIMIT_WITH_ACTION(
            "SELECT * FROM `StaffLogs` WHERE name = ? AND action = ? ORDER BY `id` DESC LIMIT ?"
    ),
    SELECT_ALL_LOGS_LIMIT(
            "SELECT * FROM `StaffLogs` WHERE name = ? ORDER BY `id` DESC LIMIT ?"
    ),
    SELECT_ALL_LOGS(
            "SELECT * FROM `StaffLogs` WHERE name = ? ORDER BY `id` DESC"
    );

    private String mysql;

    SQLLogsQuery(String mysql) {
        this.mysql = mysql;
    }

    @Override
    public String toString() {
        return mysql;
    }

    @Override
    public String getString() {
        return mysql;
    }
}
