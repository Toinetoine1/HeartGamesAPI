package fr.heartgames.heartgamesapi.moderation;

import fr.heartgames.heartgamesapi.moderation.utils.PunishmentType;

import java.util.Set;

/**
 * Created by Toinetoine1 on 23/08/2019.
 */

public class InterimData {

    private final String name;
    private final Set<Punishment> punishments, history;

    public InterimData(String name, Set<Punishment> punishments, Set<Punishment> history) {
        this.name = name;
        this.punishments = punishments;
        this.history = history;
    }

    public Punishment getBan() {
        for (Punishment pt : punishments) {
            System.out.println("punishments: "+pt.getStart()+" "+pt.isExpired());
            if (pt.getType().getFrom() == PunishmentType.BAN && !pt.isExpired()) {
                return pt;
            }
        }
        return null;
    }

    public void accept() {
        PunishmentManager.get().getLoadedPunishments(false).addAll(punishments);
        PunishmentManager.get().getLoadedHistory().addAll(history);
        PunishmentManager.get().addCached(name);
    }

}
