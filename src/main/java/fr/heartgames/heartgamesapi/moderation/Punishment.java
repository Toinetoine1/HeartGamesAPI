package fr.heartgames.heartgamesapi.moderation;

import fr.heartgames.heartgamesapi.moderation.utils.PunishmentType;
import fr.heartgames.heartgamesapi.moderation.utils.SQLModerationQuery;
import fr.heartgames.heartgamesapi.tech.sql.DatabaseAccess;
import fr.heartgames.heartgamesapi.tech.sql.SQLFactory;
import fr.heartgames.heartgamesapi.utils.general.TimeUtils;
import lombok.Getter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Toinetoine1 on 22/08/2019.
 */

public class Punishment {

    @Getter
    private final String name, operator, reason;
    @Getter
    private final long start, end;
    @Getter
    private final PunishmentType type;

    @Getter
    private int id;

    public Punishment(String name, String operator, String reason, long start, long end, PunishmentType type, int id) {
        this.name = name;
        this.operator = operator;
        this.reason = reason;
        this.start = start;
        this.end = end;
        this.type = type;
        this.id = id;
    }

    public Punishment(String name, String operator, String reason, long start, long end, PunishmentType type) {
        this.name = name;
        this.operator = operator;
        this.reason = reason;
        this.start = start;
        this.end = end;
        this.type = type;
        this.id = -1;
    }

    public void create(){
        try {
            SQLFactory.executeStatement(SQLModerationQuery.INSERT_PUNISHMENT_HISTORY, getName(), getReason(), getOperator(), getType().name(), getStart(), getEnd());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(getType() != PunishmentType.KICK){
            try {
                SQLFactory.executeStatement(SQLModerationQuery.INSERT_PUNISHMENT, getName(), getReason(), getOperator(), getType().name(), getStart(), getEnd());
                Connection connection = DatabaseAccess.getInstance().getConnection();
                PreparedStatement statement = SQLFactory.executeResultStatement(SQLModerationQuery.SELECT_EXACT_PUNISHMENT, connection, getName(), getStart());
                ResultSet rs = statement.executeQuery();
                if (rs.next()) {
                    id = rs.getInt("id");
                } else {
                    System.out.println("Error on executing a request ! Be careful !");
                }
                rs.close();
                statement.close();
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public boolean isExpired(){
        System.out.println("temp: "+getType().isTemp());
        System.out.println("end :"+ getEnd());
        System.out.println("time:"+ TimeUtils.getTime());
        System.out.println(getEnd() <= TimeUtils.getTime());
        return getType().isTemp() && getEnd() <= TimeUtils.getTime();
    }

    public void delete() {
        delete(true);
    }

    public void delete(boolean removeCache) {
        if (getType() == PunishmentType.KICK) {
            return;
        }

        if (id == -1) {
            System.out.println("ID Not found !");
            return;
        }

        try {
            SQLFactory.executeStatement(SQLModerationQuery.DELETE_PUNISHMENT, getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (removeCache) {
            PunishmentManager.get().getLoadedPunishments(false).remove(this);
        }
    }
}
