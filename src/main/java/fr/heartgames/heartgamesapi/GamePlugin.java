package fr.heartgames.heartgamesapi;

import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.api.scoreboard.CustomObjective;
import fr.heartgames.heartgamesapi.jsonconfig.DatabaseCredentials;
import fr.heartgames.heartgamesapi.jsonconfig.MongoCredentials;
import fr.heartgames.heartgamesapi.jsonconfig.RedisCredentials;
import fr.heartgames.heartgamesapi.jsonconfig.ServerConfig;
import fr.heartgames.heartgamesapi.listeners.ChatListener;
import fr.heartgames.heartgamesapi.listeners.TabGroupListener;
import fr.heartgames.heartgamesapi.loader.ModularLoader;
import fr.heartgames.heartgamesapi.logs.listeners.PacketPlayerLogListener;
import fr.heartgames.heartgamesapi.permissions.PermissionManager;
import fr.heartgames.heartgamesapi.players.GameCustomObjective;
import fr.heartgames.heartgamesapi.players.listener.PacketGroupUpdateListener;
import fr.heartgames.heartgamesapi.players.listener.PacketPlayerDataListener;
import fr.heartgames.heartgamesapi.players.packets.PacketPlayerData;
import fr.heartgames.heartgamesapi.run.RunType;
import fr.heartgames.heartgamesapi.servers.Server;
import fr.heartgames.heartgamesapi.servers.ServerManager;
import fr.heartgames.heartgamesapi.servers.listeners.PacketOnlinePlayersListener;
import fr.heartgames.heartgamesapi.servers.listeners.PacketServerNameListener;
import fr.heartgames.heartgamesapi.servers.listeners.PacketServerUpdateListener;
import fr.heartgames.heartgamesapi.servers.packets.PacketServerName;
import fr.heartgames.heartgamesapi.servers.packets.PacketServerOnlinePlayers;
import fr.heartgames.heartgamesapi.tech.mongodb.connection.MongoAccess;
import fr.heartgames.heartgamesapi.tech.redis.connection.RedisAccess;
import fr.heartgames.heartgamesapi.tech.redis.messaging.Redis;
import fr.heartgames.heartgamesapi.tech.sql.DatabaseAccess;
import fr.heartgames.heartgamesapi.utils.Utils;
import fr.heartgames.heartgamesapi.utils.bossbar.BossBarAPI;
import fr.heartgames.heartgamesapi.utils.general.BukkitUtils;
import fr.heartgames.heartgamesapi.utils.general.GsonUtils;
import fr.heartgames.heartgamesapi.utils.general.YMLUtils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Toinetoine1 on 03/08/2019.
 */

public class GamePlugin extends GameAPI {

    public static final String CONFIG_DATABASE_REDIS = "database/database_redis.json";
    public static final String CONFIG_DATABASE_SQL = "database/database_sql.json";
    public static final String CONFIG_DATABASE_MONGODB = "database/database_mongodb.json";
    public static final String UPDATE_API_URL = "https://repo.heartgames.fr/api/HeartGamesAPI.jar";
    private static final String RUNTYPE = "RunType.yml";

    @Getter
    private Server gameServer;

    @Override
    public void onEnable() {
        gameAPI = this;
        System.out.println("[HeartGamesAPI] Enabling API..");
        ModularLoader.moduled = this;

        if(!getDataFolder().exists()){
            getDataFolder().mkdir();

            new File(getDataFolder() + "/database/").mkdir();
        }

        saveDefaultConfig();

        try {
            Utils.info("&b[GameAPI] &aLoading API...");

            RedisCredentials redisCredentials = GsonUtils.load(new File(getDataFolder(), CONFIG_DATABASE_REDIS), RedisCredentials.class);
            DatabaseCredentials databaseCredentials = GsonUtils.load(new File(getDataFolder(), CONFIG_DATABASE_SQL), DatabaseCredentials.class);
            MongoCredentials mongoCredentials = GsonUtils.load(new File(getDataFolder(), CONFIG_DATABASE_MONGODB), MongoCredentials.class);
            ServerConfig serverConfig = GsonUtils.load(new File(getDataFolder(), "ServerConfig.json"), ServerConfig.class);

            Utils.info("&b[GameAPI] &aLoading databases...");
            Utils.info("&aLoading Redis Client...");
            RedisAccess.init(redisCredentials);
            Utils.info("&aLoading SQL Client...");
            DatabaseAccess.init(databaseCredentials);
            Utils.info("&aLoading MongoDB Client...");
            MongoAccess.init(mongoCredentials);

            Utils.info("&b[GameAPI] &aDatabases loaded");

            Utils.info("&b[GameAPI] &aEnabling Permission Manager...");
            new PermissionManager();

            Utils.info("&b[GameAPI] &aRegistering listeners and commands...");
            BukkitUtils.instanciateListenersAndCommandsFrom(this, "fr.heartgames.heartgamesapi.listeners", "fr.heartgames.heartgamesapi.commands");
            getServer().getPluginManager().registerEvents(new BossBarAPI(), this);
            if(getConfig().getBoolean("format_chat"))
                ChatListener.enabled = true;

            Utils.info("&b[GameAPI] &aRegistering All Packet Listeners...");
            Redis.registerPacketClassInDirectory(PacketPlayerData.PACKET_TAG, PacketPlayerData.class);
            Redis.registerPacketClassInDirectory(PacketServerOnlinePlayers.Request.PACKET_TAG, PacketServerOnlinePlayers.Request.class);
            Redis.registerPacketClassInDirectory(PacketServerName.Request.PACKET_TAG, PacketServerName.Request.class);
            Redis.registerPacketReceiver(new PacketPlayerDataListener());
            Redis.registerPacketReceiver(new PacketGroupUpdateListener());
            Redis.registerPacketReceiver(new PacketOnlinePlayersListener());
            Redis.registerPacketReceiver(new PacketServerUpdateListener());
            Redis.registerPacketReceiver(new PacketServerNameListener());
            Redis.registerPacketReceiver(new PacketPlayerLogListener());

            Utils.info("&b[GameAPI] &aCreating/Loader server in Redis...");
            gameServer = ServerManager.loadServer(serverConfig);

            Utils.info("&b[GameAPI] &aRegistering Messengers...");
            getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

            Utils.info(" ");
            Utils.info("&c[HeartGamesAPI] Loaded");
        } catch (Exception e){
            e.printStackTrace();
            Bukkit.shutdown();
        }
    }

    @Override
    public void onDisable() {
        Bukkit.getOnlinePlayers().forEach(player -> player.kickPlayer("§cCe serveur est en train de s'éteindre..."));

        ServerManager.shutdown(gameServer);

        RedisAccess.close();
        DatabaseAccess.close();
        MongoAccess.close();
    }

    @Override
    public RunType getRunType() {
        RunType runType = RunType.valueOf(YMLUtils.loadFile(new File(getDataFolder(), RUNTYPE)).getString("RunType"));
        if(runType == RunType.DEV)
            ModularLoader.dev = true;

        return runType;
    }

    @Override
    public List<HeartGamesPlayer> getOnlinePlayers() {
        return Bukkit.getOnlinePlayers().stream().map(player -> (HeartGamesPlayer) player).collect(Collectors.toList());
    }

    @Override
    public void formatChat(boolean enabled) {
        ChatListener.enabled = enabled;
    }

    @Override
    public void enableTabList(boolean enabled) {
        TabGroupListener.enabled = enabled;
    }

    @Override
    public CustomObjective buildCustomObjective(HeartGamesPlayer player) {
        return new GameCustomObjective(player);
    }

}
