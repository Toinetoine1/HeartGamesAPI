package fr.heartgames.heartgamesapi.inventory;

/*
    Created by Toinetoine1 on 25/04/2019
*/

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagInt;
import net.minecraft.server.v1_8_R3.NBTTagList;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Icon {

    final ItemStack itemStack;
    private final List<ClickAction> clickActions = new CopyOnWriteArrayList<>();

    public Icon(Material material){
        this(new ItemStack(material));
    }

    public Icon(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public Icon addClickAction(ClickAction clickAction) {
        this.clickActions.add(clickAction);
        return this;
    }

    public List<ClickAction> getClickActions() {
        return this.clickActions;
    }

    public void addAmount(int amount){
        int totalAmount = itemStack.getAmount() + amount;
        itemStack.setAmount(totalAmount > 64 ? 64 : totalAmount);
    }

    public void removeAmount(int amount){
        int totalAmount = itemStack.getAmount() - amount;
        itemStack.setAmount(totalAmount < 1 ? 1 : totalAmount);
    }

    public int getAmount(){
        return itemStack.getAmount();
    }

    public void setLore(List<String> lore){
        ItemMeta meta = itemStack.getItemMeta();
        meta.setLore(lore);
        itemStack.setItemMeta(meta);
    }

    public void setGlow(boolean glow){
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();

        if(glow){
            NBTTagList ench = new NBTTagList();
            ench.add(new NBTTagInt(1));
            ench.add(new NBTTagInt(1));

            tag.set("Enchantments", ench);
        } else {
            tag.remove("Enchantments");
        }

        nmsStack.setTag(tag);
    }
}
